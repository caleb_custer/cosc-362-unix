from ReportLibrary import *


if len(sys.argv) !=2:
	print("To Run type: python Report.py Time.log")
else:
	print "Using the time information from the file", str(sys.argv[1])
	dataFile = str(sys.argv[1])

TimeData = open(dataFile, "r")

print "Reading from file " + dataFile

TimeInformation =[]
for line in TimeData.readlines():
	Day, Hour, Minute = re.split('\d' + ' ')
	CurrentTime = Time(Day, Hour, Minute)
	TimeInformation.append(CurrentTime)


