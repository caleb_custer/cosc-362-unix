import sys
import os
import math
import re


class Time:
	def __init__(self,Day,Hour,Minute):
		self.Day    = Day
		self.Hour   = Hour
		self.Minute = Minute

class PrettyReport:
  def __init__(self,time):
    self.time = time
    
    self.header = ("\\documentclass[12pt]{article} \n"
                     + "\\usepackage{geometry} \n"
                     + "\\geometry{hmargin={1in,1in},vmargin={2in,1in}}\n"
                     + "\\begin{document} \n"
                     + "\\thispagestyle{empty} \n\n " )
    self.timeReport = "Weekly Time Report \n\n\\vskip.5in \n\n"
    
    self.weeklyAdd = (self.time.Day + " " + self.time.Hour + "\n\n"
                          + self.time.Minute + "\n\n")
    self.footer    = "\\end{document}\n"
    
    def WriteReport(self):
        ReportOutName = str(self.address.Minute.lstrip(' ')) + ".tex"
        ReportFile = open(ReportOutName,"w")
        ReportFile.write(self.header)
        ReportFile.write(self.timeReport)
        ReportFile.write(self.weeklyAdd)
        ReportFile.write(self.footer)
        ReportFile.close()