#!/bin/bash
echo Searching for tex files
ls -r *.tex
pdflatex *.tex

echo Removing .log and .aux files
ls *.log
ls *.out
ls *.aux
rm *.log
rm *.out
rm *.aux

echo Name of File = *.pdf > LatexCompileReport.txt 
 wc -w *.pdf >> LatexCompileReport.txt

echo Done


