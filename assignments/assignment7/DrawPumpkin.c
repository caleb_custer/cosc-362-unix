#include <stdio.h>
#include <stdlib.h>
#include "HelperFunctions.h"
int main(int argc,
         char *argv[]
        ){

    int numRows;
    int numCols; 
    int imageSize;
    int row, col;
    int radius;   
    int InOut;      
    int InOutStem;
    int InOutMouth;
    int InOutNose;
    unsigned char *outImage; 
    unsigned char *ptr;     
    FILE *outputFP;           

    printf("=====================================\n");  
    printf("I'm making a plain Pixel Map!        \n");
    printf("=====================================\n\n");

    if(argc!=5){
        printf("Usage: ./DrawPumpkin.c OUTfileName numrow numcols radius\n");
	printf("All tests were done using 300 300 100 as numrow, numcols and radius\n");
        exit(1);
    }
    if ( (numRows = atoi(argv[2]) ) <= 0){
        printf("Error: numRows needs to be positive");
    } 
    if ( (numCols = atoi(argv[3]) ) <= 0){
        printf("Error: numCols needs to be positive");
    }
    if ( (radius = atoi(argv[4]) ) <= 0){
        printf("Error: The radius should be positive");
    } 

  
    imageSize = numRows*numCols*3;  
    outImage  = (unsigned char *) malloc(imageSize); 


    if((outputFP = fopen(argv[1], "w")) == NULL){
        perror("output open error");
        printf("Error: can not open output file\n");
        exit(1);
    } 
    

    ptr = outImage; 

    for(row = 0; row < numRows; row++){
        for(col = 0; col < numCols; col++){
           
            InOut = InCircle(numRows, numCols, radius, row, col);
 	    InOutStem = InStem(numRows, numCols, radius, row, col);
            InOutMouth = InMouth(numRows, numCols, radius, row, col);
            InOutNose  = InNose(numRows, numCols, radius, row, col);

            if (InOut == 1){ 
               *ptr     = 225;
               *(ptr+1) = 128; 
               *(ptr+2) = 0;
            } else if (InOut == 2) {
		*ptr     = 0;
		*(ptr+1) = 0;
		*(ptr+2) = 0;
	    } else if (InOutStem == 2) {
	        *ptr     = 0;
                *(ptr+1) = 255;
                *(ptr+2) = 0;		
	    } else {
               *ptr     = 255; 
               *(ptr+1) = 255; 
               *(ptr+2) = 255; 
            }
	    if (InOutMouth == 3) {
	       *ptr     = 0;
	       *(ptr+1) = 0;
	       *(ptr+2) = 0;
	    }
	    if (InOutNose == 1) {
	       *ptr     = 0;
	       *(ptr+1) = 0;
	       *(ptr+2) = 0;
	    }
            ptr += 3; 
        }
    }
 
    fprintf(outputFP, "P6 %d %d 255\n", numCols, numRows);
    fwrite(outImage, 1, imageSize, outputFP);
    fclose(outputFP);

    return 0;
}
